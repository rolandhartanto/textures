//#version 120
//
// simple.frag
//
//uniform mat4 mat;
uniform sampler2D texture0;
uniform vec3 lightcolor;
uniform vec3 lightpos;
varying vec3 normal;
varying vec4 color;
varying vec2 texcoord;
varying vec3 pos; 
varying vec3 viewdir;
varying vec3 lightdir;

void main (void){
    vec3 light = lightpos- pos;
    vec4 texcolor = texture2D(texture0, texcoord)*2.0-1.0;

    gl_FragColor = vec4(
      (max(dot(normalize(normal+1.2* texcolor.xyz), normalize(lightdir)),0.0)+0.1)*color.xyz*lightcolor, 1.0);
}
