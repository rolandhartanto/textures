//#version 120
//
// simple.frag
//
//uniform mat4 mat;
uniform sampler2D texture0;
uniform vec3 lightcolor;
uniform vec3 lightpos;
varying vec3 normal;
varying vec4 color;
varying vec2 texcoord;
varying vec3 pos; 

void main (void){
    vec3 light = lightpos- pos;
    vec4 texcolor = texture2D(texture0, texcoord);

    gl_FragColor = vec4(
      (max(dot(normalize(normal+0.2*texcolor.xyz), normalize(light)),0.0)+0.25)*color.xyz*lightcolor, 1.0);
}
