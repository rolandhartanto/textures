//#version 120
//
// simple.frag
//
//uniform mat4 mat;
uniform sampler2D texture0;
uniform vec3 lightpos;
uniform vec3 lightcolor;
varying vec3 normal;
varying vec4 color;
varying vec2 texcoord;
varying vec3 position;

void main (void)
{
  vec3 light = lightpos- position;
  vec3 reflection = reflect(-normalize(light),normalize(normal));
  float kd = dot(normalize(normal),normalize(light));
  vec4 texcolor = texture2D(texture0, texcoord);
  gl_FragColor = vec4(texcolor.xyz*lightcolor*color.xyz*kd
                      +pow(max(dot(normalize(reflection),-normalize(position)),0.0),20.0),0.6);
}

