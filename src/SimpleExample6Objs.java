import com.jogamp.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.PMVMatrix; 
import java.awt.event.*;

public class SimpleExample6Objs extends SimpleExampleBase{
  Object3D[] objs;
  PMVMatrix mats;
  float t=0;
  Vec3 lightpos;
  Vec3 lightcolor;

  public SimpleExample6Objs(){
    super("19M38254 - Roland Hartanto - CG7", 800, 800);
    objs = new Object3D[7];
    // Chalk
    objs[0] = new Cylinder2(18,0.05f,0.5f,true, new Vec4(0.9f,0.9f,0.9f,1.0f),
                            new ImageLoader("resource/image/NormalMap.png"));
    // Brick
    objs[1] = new Cylinder2(4,0.5f,0.8f,false,  new Vec4(64f/255f,209f/255f,142f/255f,1.0f),
                            new ImageLoader("resource/image/BrickNormalMap.png"));
    // 10 Yen Coin
    objs[2] = new Coin(36,0.35f,0.035f, new Vec4(0.5f,74f/255f,0f,1.0f),
                            new ImageLoader("resource/image/10YenNormalMap.png"));
    // 10000 Yen Money Paper
    objs[3] = new Cylinder2(2,0.605f,0.6f,false, new Vec4(1.0f,1.0f,1.0f,0.0f),
                            new ImageLoader("resource/image/yen.png"));
    // Frost plastic ruler
    objs[4] = new Cylinder2(2,1.1f,0.25f,false, new Vec4(1.0f,1.0f,1.0f,0.0f),
                            new ImageLoader("resource/image/rulerlong.png"));
    // Can Piggy bank
    objs[5] = new PiggyBank(36,0.3f,0.8f, new Vec4(1.0f,1.0f,1.0f,1.0f),
                            new ImageLoader("resource/image/piggybank2.png"));
    // obj = new BezierPatch();
    mats = new PMVMatrix();
    addKeyListener(new simpleExampleKeyListener());
    addMouseMotionListener(new simpleExampleMouseMotionListener());
    addMouseListener(new simpleExampleMouseListener());
  }

  public void init(GLAutoDrawable drawable){
    drawable.setGL(new DebugGL2(drawable.getGL().getGL2()));
    final GL2 gl = drawable.getGL().getGL2();
    gl.glViewport(0, 0, SCREENW, SCREENH);

    // Clear color buffer with black
    gl.glClearColor(0.7f, 0.7f, 0.5f, 1.0f);
    gl.glClearDepth(1.0f);
    gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
    gl.glEnable(GL2.GL_DEPTH_TEST);
    gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT,1);
    gl.glFrontFace(GL.GL_CCW);
    gl.glEnable(GL.GL_CULL_FACE);
    gl.glCullFace(GL.GL_BACK);
    Shader shader0 =new Shader("resource/shader/chalk.vert",
                               "resource/shader/chalk.frag");
    Shader shader1 =new Shader("resource/shader/bumpmapping.vert",
                               "resource/shader/bumpmapping.frag");
    Shader shader2 =new Shader("resource/shader/simple.vert",
                               "resource/shader/simple.frag");
    Shader shader3 =new Shader("resource/shader/simpleshade.vert",
                               "resource/shader/simpleshade.frag");
    Shader shader4 =new Shader("resource/shader/simpletexture.vert",
                               "resource/shader/simpletexture.frag");
    Shader shader5 =new Shader("resource/shader/brick.vert",
                               "resource/shader/brick.frag");
    Shader shader6 =new Shader("resource/shader/money.vert",
                               "resource/shader/money.frag");
    Shader shader7 =new Shader("resource/shader/transplastic.vert",
                               "resource/shader/transplastic.frag");
    Shader shader8 =new Shader("resource/shader/metal.vert",
                               "resource/shader/metal.frag");
    
    shader0.init(gl);
    shader1.init(gl);
    shader2.init(gl);
    shader3.init(gl);
    shader4.init(gl);
    shader5.init(gl);
    shader6.init(gl);
    shader7.init(gl);
    shader8.init(gl);

    objs[0].init(gl, mats, shader0);
    objs[1].init(gl, mats, shader5);
    objs[2].init(gl, mats, shader1);
    objs[3].init(gl, mats, shader6);
    objs[4].init(gl, mats, shader7);
    objs[5].init(gl, mats, shader8);
    
    //objs.init(gl, mats, null);
    gl.glUseProgram(0);
    lightpos = new Vec3(0f, 5f, 20f);
    lightcolor = new Vec3(1f, 1f, 1f);
  }

  public void display(GLAutoDrawable drawable){
    final GL2 gl = drawable.getGL().getGL2();
    if(t<360){
      t = t+0.3f;
    }else{
      t = 0f;
    }
    gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
    mats.glMatrixMode(GL2.GL_PROJECTION);
    mats.glLoadIdentity();
    mats.glFrustumf(-0.5f, 0.5f, -0.5f, 0.5f, 1f, 100f);
    mats.glMatrixMode(GL2.GL_MODELVIEW);
    mats.glLoadIdentity();
    mats.glTranslatef(0f,0f,-4.0f);

    mats.glPushMatrix();
    mats.glTranslatef((0%3)-1f,0.7f-(0/3)*1.4f,0f);
    mats.glRotatef(t,0.8f,0.1f,0f);
    mats.glRotatef(90,1f,1f,-0.3f);
    mats.glRotatef(45,1f,0f,1f);
    mats.update();
    objs[0].display(gl, mats, lightpos, lightcolor);
    mats.glPopMatrix();

    mats.glPushMatrix();
    mats.glTranslatef((1%3)-1f,0.7f-(1/3)*1.4f,0f);
    mats.glRotatef(t,0f,0.1f,0f);
    mats.glRotatef(90,1f,1f,0f);
    mats.glRotatef(45,1f,0f,1f);
    mats.update();
    objs[1].display(gl, mats, lightpos, lightcolor);
    mats.glPopMatrix();

    mats.glPushMatrix();
    mats.glTranslatef((2%3)-1f,0.7f-(2/3)*1.4f,0f);
    mats.glRotatef(t,0.8f,0.1f,0f);
    mats.glRotatef(90,1f,1f,-0.3f);
    mats.glRotatef(45,1f,0f,1f);
    mats.update();
    objs[2].display(gl, mats, lightpos, lightcolor);
    mats.glPopMatrix();

    mats.glPushMatrix();
    mats.glTranslatef((3%3)-1f,0.7f-(3/3)*1.4f,0f);
    mats.glRotatef(t,0.8f,0.1f,0f);
    mats.glRotatef(90,1f,1f,0f);
    mats.glRotatef(90,1f,0f,1f);
    mats.update();
    objs[3].display(gl, mats, lightpos, lightcolor);
    mats.glPopMatrix();

    mats.glPushMatrix();
    mats.glTranslatef((5%3)-1f,0.7f-(5/3)*1.4f,0f);
    mats.glRotatef(t,0f,-0.1f,0f);
    mats.glRotatef(90,1f,1f,-0.3f);
    mats.glRotatef(45,1f,0f,1f);
    mats.update();
    objs[5].display(gl, mats, lightpos, lightcolor);
    mats.glPopMatrix();

    mats.glPushMatrix();
    mats.glTranslatef((4%3)-1f,0.7f-(4/3)*1.4f,0.5f);
    mats.glRotatef(t,1f,0.9f,0f);
    mats.glRotatef(-90,1f,1f,0f);
    mats.glRotatef(45,1f,0f,1f);
    mats.update();
    objs[4].display(gl, mats, lightpos, lightcolor);
    mats.glPopMatrix();
  
    gl.glFlush();
  }

  public static void main(String[] args){
    new SimpleExample6Objs().start();
  }
  
  class simpleExampleKeyListener implements KeyListener{
    public void keyPressed(KeyEvent e){
      int keycode = e.getKeyCode();
      System.out.print(keycode);
      if(java.awt.event.KeyEvent.VK_LEFT == keycode){
        System.out.print("a");
      }
    }
    public void keyReleased(KeyEvent e){
    }
    public void keyTyped(KeyEvent e){
    }
  }

  class simpleExampleMouseMotionListener implements MouseMotionListener{
    public void mouseDragged(MouseEvent e){
      System.out.println("dragged:"+e.getX()+" "+e.getY());
    }
    public void mouseMoved(MouseEvent e){
      System.out.println("moved:"+e.getX()+" "+e.getY());
    }
  }

  class simpleExampleMouseListener implements MouseListener{
    public void mouseClicked(MouseEvent e){
    }
    public void mouseEntered(MouseEvent e){
    }
    public void mouseExited(MouseEvent e){
    }
    public void mousePressed(MouseEvent e){
      System.out.println("pressed:"+e.getX()+" "+e.getY());
    }
    public void mouseReleased(MouseEvent e){
    }
  }
}
